# Role Name

monitoring.nagiosbase

# Description

Ansible role to generate Nagios like configuration for client from templates.

The server is the monitoring server. The  client is the monitored server,
take care that the NRPE server is on each client.

# Status

Alpha, this is a work in progress, only tested with Ubuntu 20.04 LTS

# Requirements

It's useless to use this module if you don't have at least a Nagios-like server.

# Role Variables

Some variables are needed, as location differ between distribution.
And your may want to define your own.

etc\_config\_nrpe: nrpe configuration file (nrpe.cfg).
nrpe\_d\_dir: nrpe.d/ directory with other configuration.
nrpe\_owner: nrpe user
nrpe\_service\_name: nrpe service name.
etc\_vars\_nrpe: nrpe service configuration file.
path\_monitoring\_script\_1: directory for nagios like checks.

monitoring\_servers\_ips: monitoring server IPs list.

# Dependencies

None

# Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: monitoring.nagiosbase }

# License

GNU General Public License v3.0 or later

See `COPYING <COPYING>`_ to see the full text.

# Author Information

Some people
